<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lists".
 *
 * @property int $id
 * @property string $fullname
 * @property string $email
 * @property string $states
 * @property string $event
 * @property string $rooms
 * @property string $date
 * @property string $created_date
 */
class Lists extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lists';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'email', 'states', 'event_category', 'seat_number', 'date'], 'required'],
          
            [['full_name', 'email', 'states', 'event_category', 'seat_number', 'date'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Fullname',
            'email' => 'Email',
            'states' => 'States',
            'event_category' => 'Event Category',
            'seat_number' => 'Seat Number',
            'date' => 'Date',
            'created_dated' => 'Created Date',
        ];
    }
	public function getUser(){

return $this->hasOne(User::className(), ['id' => 'user_id']);


}
}
