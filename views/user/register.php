<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
?>
<div class="user-register">
	 <h2 class="page-header">Register Here</h2>

    <?php $form = ActiveForm::begin(); ?>
	<?= $form->errorSummary($model); ?>

        <?= $form->field($model, 'full_name') ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'email') ?>
<?= $form->field($model, 'Gender')->radioList(['male' =>'Male', 'female' =>'Female']) ?>   
	<?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'password_repeat')->passwordInput() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- user-register -->
