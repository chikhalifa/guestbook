<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textarea(['rows' => 1]) ?>

 <?=
    $form->field($model, 'states')->dropDownList(
                    ['lagos' => 'Lagos', 'delta' => 'Delta', 'edo' => 'Edo', 'abuja' => 'Abuja', 'ogun' =>'Ogun', 'imo'=> 'Imo'],
                    ['prompt' => 'Select Your Location', 'id' => 'bar']
                );
     ?>   
    

    <?=
    $form->field($model, 'event')->dropDownList(
                    ['regular' => 'Regular', 'vip' => 'Vip', 'vvip' => 'Vvip', 'private' => 'Private'],
                    ['prompt' => 'Select Event Type', 'id' => 'bar']
                );
     ?>
             
    <?= $form->field($model, 'rooms')->textInput() ?>
    <?= $form->field($model, 'date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
