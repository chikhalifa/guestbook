 <?php
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>

<h2 class="pager-header"><marquee><strong>Guest List</strong> </marquee><a class="btn btn-warning pull-right" href="/guestbook/web/index.php?r=lists/create">create</a></h2>
<?php  if(null !== Yii::$app->session->getFlash('success')) : ?>

<div class="alert alert-success"><?php echo Yii::$app->session->getFlash('success'); ?></div>
<?php endif; ?>

<?php if(!empty($lists) ) : ?>
	
<ul class="list-group">
	<table class="table">
		<tr style="background-color: green;color:pink; font-size:18px; font-family:normal; font-weight:bold;" >
		     <th style="background-color: gray;color: ;">S/N</th>
              <th style="background-color: green; color: ;">fullname</th>
              <th style="background-color: gray;color: ;">email</th>
              <th style="background-color: green;color: ;">states</th>
              <th style="background-color: gray;color: ;">event category</th>
              <th style="background-color: green;color: ;" >seat no.</th>
		  <th style="background-color: green;color: ;">created date</th>

		    
            </tr>
<?php foreach ($lists as $list): ?>
    
            

        <tr>		      
       <!--  -->
		 <td style="background-color: gray;color: white; font-size:16px; font-family:cursive; font-weight:bold;"> <li class="list-group-item  "><a href="/guestbook/web/index.php?r=lists/details&id=<?php echo $list->id ?>"><?=$list->id?></a></td>

		 <td style="background-color: green; color: white; font-size:16px; font-family:cursive; font-weight:bold;"><li class="list-group-item  "><a href="/guestbook/web/index.php?r=lists/details&id=<?php echo $list->id ?>"> <?=$list->full_name?> </a></td>
		 <td style="background-color: gray;color: white;font-size:16px; font-family:cursive; font-weight:bold;"><li class="list-group-item  "><a href="/guestbook/web/index.php?r=lists/details&id=<?php echo $list->id ?>"> <?=$list->email?></a>
 </td>
		 <td style="background-color: green;color: white;font-size:16px; font-family:cursive; font-weight:bold;"> <li class="list-group-item  "><a href="/guestbook/web/index.php?r=lists/details&id=<?php echo $list->id ?>"><?=$list->states?> </a></td>
		 <td style="background-color: gray;color: white;font-size:16px; font-family:cursive; font-weight:bold;"> <li class="list-group-item  "><a href="/guestbook/web/index.php?r=lists/details&id=<?php echo $list->id ?>"><?=$list->event_category?> </a></td>
		 <td style="background-color: green;color: white;font-size:16px; font-family:cursive; font-weight:bold;"><li class="list-group-item  "><a href="/guestbook/web/index.php?r=lists/details&id=<?php echo $list->id ?>"><?=$list->seat_number?></a> </td>
           <?php $phpdate = strtotime($list->created_date);?>
	<?php $formatted_date = date("F j, Y , g:i a", $phpdate);?>
		 <td style="background-color: green;color: white;font-size:16px; font-family:cursive; font-weight:strong;"><strong><?php echo $formatted_date; ?> <?=$list->created_date?></strong> </td>






<!-- 
                    <td>{ $list->email }</td>
                    <td>{$email}</td>
                    <td>{$states}</td>
                    <td>{$event}</td>
                    <td>{$rooms}</td>
		      <td>{$date}</td>
		      <td>{$created_date}</td> -->
                    
                    </tr>
   
<?php endforeach; ?>
 </table>

<?php else :?>
	<P>NO GUEST LISTED</P>
	<?php endif; ?>	
</ul>

		<?= LinkPager::widget(['pagination' => $pagination]) ?>

