<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DatePicker;
use yii\helpers\ArrayHelper;

?>

<div class="list-form">
    <h2 class="page-header">Create Guest List</h2>

    <?php $form = ActiveForm::begin(); ?>
    	<?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textarea(['rows' => 1]) ?>

 <?=
    $form->field($model, 'states')->dropDownList(
                    ['lagos' => 'Lagos', 'delta' => 'Delta', 'edo' => 'Edo', 'abuja' => 'Abuja', 'ogun' =>'Ogun', 'imo'=> 'Imo'],
                    ['prompt' => 'Select Your Location', 'id' => 'bar']
                );
     ?>   
    

    <?=
    $form->field($model, 'event_category')->dropDownList(
                    ['regular' => 'Regular', 'vip' => 'Vip', 'vvip' => 'Vvip', 'private' => 'Private'],
                    ['prompt' => 'Select Event Type', 'id' => 'bar']
                );
     ?>
             
    <?= $form->field($model, 'seat_number')->textInput() ?>
	       <?= $form->field($model, 'date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
